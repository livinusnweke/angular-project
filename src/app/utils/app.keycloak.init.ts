import { KeycloakService } from "keycloak-angular";

export function initializeKeycloak(keycloak: KeycloakService):()=>Promise<boolean> {
    return () =>
      keycloak.init({
        config: {
          url: 'http://localhost:8083/auth',
          realm: 'noroff',
          clientId: 'my-app',
        },
        initOptions: {
            checkLoginIframe:true,
            checkLoginIframeInterval:25
        //   onLoad: 'check-sso',
        //   silentCheckSsoRedirectUri:
        //     window.location.origin + '/assets/silent-check-sso.html',
        },
        loadUserProfileAtStartUp:true
      });
  }
  